from django.contrib import admin
from .models import Programa
from .forms import ProgramasForm


class ProgramaAdmin(admin.ModelAdmin):
    """
        Admin for programas
    """
    date_hierarchy = 'created'
    list_display = ('title', 'horario', 'hora_inicio', 'hora_fin')
    # list_editable = ('title', 'is_published')
    #list_filter = ('dias', )
    search_fields = ['title']

    # prepopulated_fields = {'slug': ('title',)}
    form = ProgramasForm

    # inlines = [NewsImageInline, ]
    # actions = ['make_published', 'make_unpublished']

    save_as = True
    save_on_top = True

    def queryset(self, request):
        """
            Override to use the objects and not just the default
            visibles only.
        """
        return Programa.objects.all()


admin.site.register(Programa, ProgramaAdmin)
