from __future__ import absolute_import
from django import forms

from cms.plugin_pool import plugin_pool
from .models import Programa, DIAS_CHOICES
from django.forms import CheckboxSelectMultiple


class ProgramasForm(forms.ModelForm):
    dias = forms.MultipleChoiceField(
            choices=DIAS_CHOICES,
            widget=CheckboxSelectMultiple,
            label="Dias del programa",
            required=False,
            show_hidden_initial=True)

    class Meta:
        model = Programa
        fields = '__all__'

