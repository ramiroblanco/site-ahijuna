# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programas', '0003_programa_horario'),
    ]

    operations = [
        migrations.AlterField(
            model_name='programa',
            name='content',
            field=models.CharField(max_length=150, verbose_name=b'Contenido'),
        ),
        migrations.AlterField(
            model_name='programa',
            name='created',
            field=models.DateTimeField(verbose_name=b'Fecha de creacion', editable=False),
        ),
        migrations.AlterField(
            model_name='programa',
            name='hora_fin',
            field=models.TimeField(verbose_name=b'Hora fin'),
        ),
        migrations.AlterField(
            model_name='programa',
            name='hora_inicio',
            field=models.TimeField(verbose_name=b'Hora inicio'),
        ),
        migrations.AlterField(
            model_name='programa',
            name='horario',
            field=models.CharField(max_length=150, verbose_name=b'Descripcion de horario'),
        ),
        migrations.AlterField(
            model_name='programa',
            name='image',
            field=models.ImageField(upload_to=b'', verbose_name=b'Imagen del Programa'),
        ),
        migrations.AlterField(
            model_name='programa',
            name='title',
            field=models.CharField(max_length=255, verbose_name=b'Titulo'),
        ),
    ]
