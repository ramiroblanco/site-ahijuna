# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programas', '0002_auto_20160202_0122'),
    ]

    operations = [
        migrations.AddField(
            model_name='programa',
            name='horario',
            field=models.CharField(default='', max_length=150, verbose_name=b'horario'),
            preserve_default=False,
        ),
    ]
