# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
    ]

    operations = [
        migrations.CreateModel(
            name='Programa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'title')),
                ('content', models.CharField(max_length=150, verbose_name=b'content')),
                ('created', models.DateTimeField(verbose_name=b'created', editable=False)),
                ('image', models.ImageField(upload_to=b'', verbose_name=b'image')),
                ('hora_inicio', models.TimeField(verbose_name=b'inicio')),
                ('hora_fin', models.TimeField(verbose_name=b'fin')),
                ('dias', models.CharField(max_length=200, choices=[(b'L', b'LUNES'), (b'Mm', b'MARTES'), (b'M', b'MIERCOLES'), (b'J', b'JUEVES'), (b'V', b'VIERNES'), (b'S', b'SABADO'), (b'D', b'DOMINGO')])),
            ],
            options={
                'ordering': ['created'],
            },
        ),
        migrations.CreateModel(
            name='ProgramasPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
