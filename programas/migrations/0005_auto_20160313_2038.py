# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programas', '0004_auto_20160305_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='programa',
            name='horario',
            field=models.CharField(max_length=150, verbose_name=b'Descripcion de horario', blank=True),
        ),
    ]
