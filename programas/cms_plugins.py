# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from programas.models import ProgramasPlugin, Programa


class CMSProgramasPlugin(CMSPluginBase):
    """
        Plugin class for the latest news
    """
    model = ProgramasPlugin
    name = _('Programas')
    render_template = "programas/index.html"

    def render(self, context, instance, placeholder):
        """
            Render the latest news
        """
        lunes= Programa.objects.filter(dias__contains='1').order_by('hora_inicio')
        martes= Programa.objects.filter(dias__contains='2').order_by('hora_inicio')
        miercoles= Programa.objects.filter(dias__contains='3').order_by('hora_inicio')
        jueves= Programa.objects.filter(dias__contains='4').order_by('hora_inicio')
        viernes= Programa.objects.filter(dias__contains='5').order_by('hora_inicio')
        sabado= Programa.objects.filter(dias__contains='6').order_by('hora_inicio')
        domingo= Programa.objects.filter(dias__contains='7').order_by('hora_inicio')

        #DIAS_CHOICES = ((1,'LUNES'), (2,'MARTES'), (3,'MIERCOLES'), (4,'JUEVES'), (5,'VIERNES'), (6,'SABADO'), (7,'DOMINGO'))

        latest = []
        latest.append(('lunes',(lunes)))
        latest.append(('martes',(martes)))
        latest.append(('miércoles',(miercoles)))
        latest.append(('jueves',(jueves)))
        latest.append(('viernes',(viernes)))
        latest.append(('sábado',(sabado)))
        latest.append(('domingo',(domingo)))
        menor_hora_semanal = Programa.objects.order_by('hora_inicio')[0]
        #print menor_hora_semanal.hora_fin

        context.update({
            'instance': instance,
            'latest': latest,
            'placeholder': placeholder,
            'trasnoche': menor_hora_semanal,
        })
        return context


plugin_pool.register_plugin(CMSProgramasPlugin)
