from django.views import generic
from models import Programa
from django.http import HttpResponse
import json
import datetime
# Create your views here.


def actual(request):
    if request.method == 'GET':
        response_data = {}
        hoy = datetime.datetime.today()

        programa = Programa.objects.filter(dias__icontains=hoy.weekday()+1)\
            .filter(hora_inicio__lte=hoy.strftime('%H:%M:%S'))\
            .filter(hora_fin__gte=hoy.strftime('%H:%M:%S'))
        response_data['programa'] = 'Seleccion Musical'
        if programa:
            response_data['programa'] = programa[0].title
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )


class IndexView(generic.ListView):
    template_name = "programas/index.html"
    context_object_name = 'Programas'

    def get_queryset(self):

        latest = Programa.objects.all().order_by('-created')
        return latest

