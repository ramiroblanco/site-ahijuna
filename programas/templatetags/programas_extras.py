from django import template

register = template.Library()


@register.filter(name='margenizar')
def margenizar(hora_inicio, hora_anterior):
    '''devuelve el valor del margen correspondiente para la grilla de programas'''

    inicio = hora_inicio.hour*60 + hora_inicio.minute
    fin_previo = hora_anterior.hour*60 + hora_anterior.minute
    if inicio <= fin_previo:
        return 0

    diferencial = (inicio - fin_previo)*1.5 #- 30)/5 #diferencial de alto extra para programas de mas de media hora
    return int(diferencial)


@register.filter(name='calendarizar')
def calendarizar(programa):
    min_final = (programa.hora_fin.hour*60 + programa.hora_fin.minute)
    min_inicio = (programa.hora_inicio.hour*60 + programa.hora_inicio.minute)
    diferencial_minutos = min_final - min_inicio #(min_final - min_inicio - 30)/5
    return int(diferencial_minutos*1.5) # cada minuto corresponde a un pixel


@register.filter(name='formatear')
def formatear(programa):
    min_final = (programa.hora_fin.hour*60 + programa.hora_fin.minute)
    min_inicio = (programa.hora_inicio.hour*60 + programa.hora_inicio.minute)
    diferencial_minutos = min_final - min_inicio #(min_final - min_inicio - 30)/5
    if diferencial_minutos == 30:
        return 0
    return diferencial_minutos/5 # cada minuto corresponde a un pixel


@register.filter(name='prev')
def prev(list, index):
    previo = list[int(index)-1]
    return previo.hora_fin


