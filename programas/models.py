# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from cms.models import CMSPlugin
import json

DIAS_CHOICES = ((1,'LUNES'), (2,'MARTES'), (3,'MIÉRCOLES'), (4,'JUEVES'), (5,'VIERNES'), (6,'SÁBADO'), (7,'DOMINGO'))


class Programa(models.Model):

    title = models.CharField('Titulo', max_length=255)
    horario = models.CharField('Descripcion de horario', max_length=150, blank=True)
    content = models.CharField('Contenido', max_length=150)
    created = models.DateTimeField('Fecha de creacion', editable=False)
    image = models.ImageField('Imagen del Programa')
    hora_inicio = models.TimeField('Hora inicio')
    hora_fin = models.TimeField('Hora fin')

    dias = models.CharField(max_length=200)


    def setdias(self, x):
        self.dias = json.dumps(x)

    def getdias(self):
        return json.loads(self.dias)

    def __unicode__(self):              # __unicode__ on Python 2
        return self.title

    def save(self, *args, **kwargs):
        ''' On save, update timestamps and validate documents '''
        if not self.id:
            self.created = timezone.now()
        if self.title == '':
            self.title = self.image.path
        return super(Programa, self).save(*args, **kwargs)

    class Meta:
        ordering = ['created']


class ProgramasPlugin(CMSPlugin):
    '''Definicion para el elemento placeholder de programas'''
