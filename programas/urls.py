
from django.conf.urls import patterns, url
from . import views

urlpatterns = [
    url(r'^progactual$', views.actual, name='actual'),
    url(r'^$', views.IndexView.as_view()),
]
