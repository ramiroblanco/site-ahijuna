# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('djangocms_owl', '0002_owlcarousel_extra_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='owlcarousel',
            name='extra_options',
            field=jsonfield.fields.JSONField(verbose_name='JSON options', blank=True),
        ),
        migrations.AlterField(
            model_name='owlcarousel',
            name='template',
            field=models.CharField(default=b'default', max_length=255, verbose_name='plantilla', choices=[(b'default', 'Default')]),
        ),
    ]
