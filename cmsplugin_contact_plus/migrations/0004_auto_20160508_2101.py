# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmsplugin_contact_plus', '0003_auto_20160313_1519'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactplus',
            name='recipient_email',
            field=models.EmailField(default=b'postmaster@uvq.edu.ar', max_length=254, verbose_name='Email of recipients'),
        ),
    ]
