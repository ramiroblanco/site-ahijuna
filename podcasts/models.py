from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.utils import timezone

from cms.models import CMSPlugin

from . import settings


class PublishedPodcastManager(models.Manager):
    """
        Filters out all unpublished and items with a publication
        date in the future
    """
    def get_queryset(self):
        return super(PublishedPodcastManager, self).get_queryset().filter(
            is_published=True).filter(pub_date__lte=timezone.now())


class Podcast(models.Model):
    """
    Podcast
    """

    title = models.CharField(_('Titulo'), max_length=255)
    content = models.TextField(_('Contenido'), blank=True)
    file_download = models.CharField(_('Link de descarga'), max_length=255, blank=True)
    #video_url = models.CharField(_('Video URL'), max_length=255, blank=True)


    is_published = models.BooleanField(_('Visible?'), default=False)
    pub_date = models.DateTimeField(
        _('Fecha de publicacion'),
        default=timezone.now)

    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    published = PublishedPodcastManager()
    objects = models.Manager()

    class Meta:
        verbose_name = _('Podcast')
        verbose_name_plural = _('Podcasts')
        ordering = ('-pub_date', )

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        if settings.LINK_AS_ABSOLUTE_URL and self.link:
            if settings.USE_LINK_ON_EMPTY_CONTENT_ONLY and not self.content:
                return self.link
        return reverse('news_detail',
                       kwargs={'year': self.pub_date.strftime("%Y"),
                               'month': self.pub_date.strftime("%m"),
                               'day': self.pub_date.strftime("%d"),
                               'slug': self.slug})


class PodcastImage(models.Model):
    podcast = models.ForeignKey(Podcast, related_name='images')
    image = models.ImageField(upload_to="podcast_images")


class LatestPodcastPlugin(CMSPlugin):
    """
        Model for the settings when using the latest news cms plugin
    """
    limit = models.PositiveIntegerField(_('Number of news items to show (\'0\' to show all)'),
                                        help_text=_('Limits the number of '
                                                    'items that will be '
                                                    'displayed'))
