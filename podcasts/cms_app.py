from django.utils.translation import ugettext_lazy as _

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import PodcastItemMenu


class PodcastAppHook(CMSApp):
    name = _('Podcast App')
    urls = ['podcasts.urls']
    menus = [PodcastItemMenu]


apphook_pool.register(PodcastAppHook)
