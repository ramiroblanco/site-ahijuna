from django.views import generic as generic_views

from . import models
from . import settings
from django.db.models import Q
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class PublishedPodcastMixin(object):
    """
    Since the queryset also has to filter elements by timestamp
    we have to fetch it dynamically.
    """
    def get_queryset(self):
        return models.Podcast.published.all()


class ArchiveIndexView(PublishedPodcastMixin, generic_views.ListView):
    """
    A simple archive view that exposes following context:

    * latest
    * date_list
    * paginator
    * page_obj
    * object_list
    * is_paginated

    The first two are intended to mimic the behaviour of the
    date_based.archive_index view while the latter ones are provided by
    ListView.
    """
    paginate_by = settings.ARCHIVE_PAGE_SIZE
    template_name = 'podcasts/news_archive.html'
    include_yearlist = True
    date_field = 'pub_date'

    def get_context_data(self, **kwargs):
        context = super(ArchiveIndexView, self).get_context_data(**kwargs)
        context['latest'] = context['object_list']
        if self.include_yearlist:
            date_list = self.get_queryset().datetimes('pub_date', 'year')[::-1]
            context['date_list'] = date_list
        return context


def search(request):
    if request.method == "GET" and request.is_ajax():
        key = request.GET['clave'][1:-1]
        latest = []
        if not key:
            latest = models.Podcast.objects.all().order_by('-created')
        else:
            latest= models.Podcast.objects.filter(Q(title__icontains=str(key)) | Q(content__icontains=str(key))).order_by('-created')
            #latest = query.order_by('-created')
        paginator = Paginator(latest, 3)  # Show 5 contacts per page
        page = request.GET.get('page')
        try:
            latest = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            latest = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            latest = paginator.page(paginator.num_pages)
        html = render_to_string('podcasts/news_archive.html', {'latest': latest})
        return HttpResponse(html)


def mas(request):
    if request.method == "GET" and request.is_ajax():
        key = request.GET['clave'][1:-1]
        latest = []
        if not key:
            latest = models.Podcast.objects.all().order_by('-created')
        else:
            latest= models.Podcast.objects.filter(Q(title__icontains=str(key)) | Q(content__icontains=str(key))).order_by('-created')
            #latest = query.order_by('-created')
        paginator = Paginator(latest, 3)  # Show 5 contacts per page
        page = request.GET.get('page')
        try:
            latest = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            latest = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            latest = paginator.page(paginator.num_pages)
        html = render_to_string('podcasts/paginado.html', {'latest': latest})
        return HttpResponse(html)

class ArchiveSearchView(PublishedPodcastMixin, generic_views.ListView):
    """
    A simple archive view that exposes following context:

    * latest
    * date_list
    * paginator
    * page_obj
    * object_list
    * is_paginated

    The first two are intended to mimic the behaviour of the
    date_based.archive_index view while the latter ones are provided by
    ListView.
    """
    paginate_by = settings.ARCHIVE_PAGE_SIZE
    template_name = 'podcasts/news_archive.html'
    include_yearlist = True
    date_field = 'pub_date'

    def get_context_data(self, **kwargs):
        context = super(ArchiveSearchView, self).get_context_data(**kwargs)
        if self.include_yearlist:
            date_list = self.get_queryset().datetimes('pub_date', 'year')[::-1]
            context['date_list'] = date_list
        return context


class DetailView(PublishedPodcastMixin, generic_views.DateDetailView):
    month_format = '%m'
    date_field = 'pub_date'


class MonthArchiveView(PublishedPodcastMixin, generic_views.MonthArchiveView):
    month_format = '%m'
    date_field = 'pub_date'


class YearArchiveView(PublishedPodcastMixin, generic_views.YearArchiveView):
    month_format = '%m'
    date_field = 'pub_date'


class DayArchiveView(PublishedPodcastMixin, generic_views.DayArchiveView):
    month_format = '%m'
    date_field = 'pub_date'
