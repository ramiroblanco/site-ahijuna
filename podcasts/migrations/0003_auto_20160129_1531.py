# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('podcasts', '0002_auto_20150428_1335'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='excerpt',
        ),
        migrations.RemoveField(
            model_name='news',
            name='link',
        ),
        migrations.RemoveField(
            model_name='news',
            name='slug',
        ),
        migrations.AddField(
            model_name='news',
            name='pdf',
            field=models.FileField(upload_to=b'public', blank=True),
        ),
        migrations.AddField(
            model_name='news',
            name='video_url',
            field=models.CharField(max_length=255, verbose_name='Video URL', blank=True),
        ),
    ]
