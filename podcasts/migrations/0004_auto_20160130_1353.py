# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('podcasts', '0003_auto_20160129_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='latestnewsplugin',
            name='limit',
            field=models.PositiveIntegerField(help_text='Limits the number of items that will be displayed', verbose_name="Number of news items to show ('0' to show all)"),
        ),
    ]
