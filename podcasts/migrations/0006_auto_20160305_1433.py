# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('podcasts', '0005_auto_20160305_1300'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='podcast',
            name='pdf',
        ),
        migrations.AddField(
            model_name='podcast',
            name='file_download',
            field=models.CharField(max_length=255, verbose_name='Link de descarga', blank=True),
        ),
    ]
