# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('podcasts', '0004_auto_20160130_1353'),
    ]

    operations = [
        migrations.CreateModel(
            name='Podcast',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Titulo')),
                ('content', models.TextField(verbose_name='Contenido', blank=True)),
                ('pdf', models.FileField(upload_to=b'public', blank=True)),
                ('video_url', models.CharField(max_length=255, verbose_name='Video URL', blank=True)),
                ('is_published', models.BooleanField(default=False, verbose_name='Visible?')),
                ('pub_date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Fecha de publicacion')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ('-pub_date',),
                'verbose_name': 'Podcast',
                'verbose_name_plural': 'Podcasts',
            },
        ),
        migrations.CreateModel(
            name='PodcastImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'podcast_images')),
                ('podcast', models.ForeignKey(related_name='images', to='podcasts.Podcast')),
            ],
        ),
        migrations.RenameModel(
            old_name='LatestNewsPlugin',
            new_name='LatestPodcastPlugin',
        ),
        migrations.RemoveField(
            model_name='newsimage',
            name='news',
        ),
        migrations.DeleteModel(
            name='News',
        ),
        migrations.DeleteModel(
            name='NewsImage',
        ),
    ]
