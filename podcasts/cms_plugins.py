from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from podcasts.models import LatestPodcastPlugin, Podcast
from podcasts import settings


class CMSLatestPodcastPlugin(CMSPluginBase):
    """
        Plugin class for the latest news
    """
    model = LatestPodcastPlugin
    name = _('Latest news')
    render_template = "podcasts/latest_news.html"

    def render(self, context, instance, placeholder):
        """
            Render the latest news
        """
        latest = Podcast.objects.filter(is_published=True)
        if not instance.limit == 0:
            latest = latest[:instance.limit]
        context.update({
            'instance': instance,
            'latest': latest,
            'placeholder': placeholder,
        })
        return context

if not settings.DISABLE_LATEST_PODCAST_PLUGIN:
    plugin_pool.register_plugin(CMSLatestPodcastPlugin)
