
from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns(

    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^getall$',
        views.getall, name='todas_noticias'),
)
