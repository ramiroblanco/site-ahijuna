# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='background_content',
            field=models.CharField(max_length=10, verbose_name=b'Color de fondo en contenido'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='background_title',
            field=models.CharField(max_length=10, verbose_name=b'Color de fondo en titulo'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='color_content',
            field=models.CharField(max_length=10, verbose_name=b'Color de fuente en contenido'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='color_title',
            field=models.CharField(max_length=10, verbose_name=b'Color de fuente en titulo'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='content',
            field=models.CharField(max_length=150, verbose_name=b'Descripcion'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='created',
            field=models.DateTimeField(verbose_name=b'Fecha creacion', editable=False),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='image',
            field=models.ImageField(upload_to=b'', verbose_name=b'Imagen'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='title',
            field=models.CharField(max_length=255, verbose_name=b'Titulo'),
        ),
    ]
