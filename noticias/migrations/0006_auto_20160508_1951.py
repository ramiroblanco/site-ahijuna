# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0005_auto_20160313_2014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='content',
            field=models.CharField(max_length=1000, verbose_name=b'Descripcion'),
        ),
    ]
