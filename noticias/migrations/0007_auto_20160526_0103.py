# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0006_auto_20160508_1951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='background_content',
            field=models.CharField(max_length=20, verbose_name=b'Color de fondo en contenido'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='background_title',
            field=models.CharField(max_length=20, verbose_name=b'Color de fondo en titulo'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='color_content',
            field=models.CharField(max_length=20, verbose_name=b'Color de fuente en contenido'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='color_title',
            field=models.CharField(max_length=20, verbose_name=b'Color de fuente en titulo'),
        ),
    ]
