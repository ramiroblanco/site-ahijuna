# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0004_auto_20160305_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='content',
            field=models.CharField(max_length=500, verbose_name=b'Descripcion'),
        ),
    ]
