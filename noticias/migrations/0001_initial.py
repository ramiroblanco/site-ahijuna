# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
    ]

    operations = [
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'title')),
                ('content', models.CharField(max_length=150, verbose_name=b'content')),
                ('created', models.DateTimeField(verbose_name=b'created', editable=False)),
                ('image', models.ImageField(upload_to=b'', verbose_name=b'image')),
                ('color_title', models.CharField(max_length=10, verbose_name=b'font-color-title')),
                ('background_title', models.CharField(max_length=10, verbose_name=b'background-color-title')),
                ('color_content', models.CharField(max_length=10, verbose_name=b'font-color-content')),
                ('background_content', models.CharField(max_length=10, verbose_name=b'background-color-content')),
            ],
            options={
                'ordering': ['created'],
            },
        ),
        migrations.CreateModel(
            name='NoticiasPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
