# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorful.fields


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0002_auto_20160305_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='background_content',
            field=colorful.fields.RGBColorField(verbose_name=b'Color de fondo en contenido'),
        ),
    ]
