# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorful.fields


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0003_auto_20160305_1411'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='background_title',
            field=colorful.fields.RGBColorField(verbose_name=b'Color de fondo en titulo'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='color_content',
            field=colorful.fields.RGBColorField(verbose_name=b'Color de fuente en contenido'),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='color_title',
            field=colorful.fields.RGBColorField(verbose_name=b'Color de fuente en titulo'),
        ),
    ]
