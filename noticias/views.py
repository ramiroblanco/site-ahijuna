from django.views import generic
from .models import Noticia
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render

# Create your views here.


class IndexView(generic.ListView):
    template_name = "noticias/index.html"
    context_object_name = 'Noticias'

    def get_queryset(self):
        #noticias = Noticia.objects.all()

        # items = Congreso.objects.filter(pk__in=set(congresos_id)).order_by('-created')
        latest = Noticia.objects.all().order_by('created')
        return latest[:6] if len(latest) > 6 else latest


def getall(request):
    if request.method == "GET" and request.is_ajax():
        #key = request.GET['clave'][1:-1]
        latest = []
        latest = Noticia.objects.all().order_by('-created')[6:]

        paginator = Paginator(latest, 5)  # Show 5 contacts per page
        page = request.GET.get('page')
        try:
            latest = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            latest = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            latest = paginator.page(paginator.num_pages)

        #return render(request, 'list.html', {'latest': latest})

        html = render_to_string('noticias/todas_paginado.html', {'latest': latest})
        return HttpResponse(html)
