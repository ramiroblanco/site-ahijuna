from django.contrib import admin
from .models import Noticia
from .forms import NoticiasForm


class NoticiasAdmin(admin.ModelAdmin):
    """
        Admin for noticias
    """
    date_hierarchy = 'created'
    list_display = ('title', 'image', 'created')
    # list_editable = ('title', 'is_published')
    # list_filter = ('is_published', )
    search_fields = ['title']

    # prepopulated_fields = {'slug': ('title',)}
    form = NoticiasForm
    # inlines = [NewsImageInline, ]

    # actions = ['make_published', 'make_unpublished']

    save_as = True
    save_on_top = True

    def queryset(self, request):
        """
            Override to use the objects and not just the default
            visibles only.
        """
        return Noticia.objects.all()


admin.site.register(Noticia, NoticiasAdmin)
