from django.db import models
from django.utils import timezone
from cms.models import CMSPlugin
from colorful.fields import RGBColorField

COLORES_TEXTO = (('#000028', '#000028'), ('#E33903', '#E33903'), ('#43AFA8', '#43AFA8'))
COLORES_FONDO = (('#F63E00', '#F63E00'), ('#000028', '#000028'), ('#F9CE35', '#F9CE35'), ('#49BFB5', '#49BFB5'))

class Noticia(models.Model):

    title = models.CharField('Titulo', max_length=255)
    content = models.CharField('Descripcion', max_length=1000)
    created = models.DateTimeField('Fecha creacion', editable=False)
    image = models.ImageField('Imagen')
    color_title = models.CharField('Color de fuente en titulo', max_length=20)
    background_title = models.CharField('Color de fondo en titulo', max_length=20)
    color_content = models.CharField('Color de fuente en contenido', max_length=20)
    background_content = models.CharField('Color de fondo en contenido', max_length=20) #RGBColorField, colors=['#FF0000', '#00FF00', '#0000FF'])

    #modified = models.DateTimeField('modified', editable=False)

    def __unicode__(self):              # __unicode__ on Python 2
        return self.title

    def save(self, *args, **kwargs):
        ''' On save, update timestamps and validate documents '''
        if not self.id:
            self.created = timezone.now()
        if self.title == '':
            self.title = self.image.path
        return super(Noticia, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-created']


class NoticiasPlugin(CMSPlugin):
    '''Definicion para el elemento placeholder de noticias'''
