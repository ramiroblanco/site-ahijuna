from __future__ import absolute_import
from django import forms

from cms.plugin_pool import plugin_pool
from djangocms_text_ckeditor.widgets import TextEditorWidget
from .models import Noticia, COLORES_FONDO, COLORES_TEXTO


class NoticiasForm(forms.ModelForm):
    background_title = forms.ChoiceField(
            choices=COLORES_FONDO,
            label="Color de fondo del Titulo",
            required=True,
            show_hidden_initial=True)
    background_content = forms.ChoiceField(
            choices=COLORES_FONDO,
            label="Color de fondo del Contenido",
            required=True,
            show_hidden_initial=True)
    color_title = forms.ChoiceField(
            choices=COLORES_TEXTO,
            label="Color para el texto del Titulo",
            required=True,
            show_hidden_initial=True)
    color_content = forms.ChoiceField(
            choices=COLORES_TEXTO,
            label="Color para el texto del Contenido",
            required=True,
            show_hidden_initial=True)

    class Meta:
        model = Noticia
        fields = '__all__'

    def _get_widget(self):
        plugins = plugin_pool.get_text_enabled_plugins(placeholder=None,
                                                       page=None)
        return TextEditorWidget(installed_plugins=plugins)

    def __init__(self, *args, **kwargs):
        super(NoticiasForm, self).__init__(*args, **kwargs)
        widget = self._get_widget()
        #self.fields['excerpt'].widget = widget

        self.fields['content'].widget = widget
