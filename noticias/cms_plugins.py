from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import NoticiasPlugin, Noticia


class CMSNoticiasPlugin(CMSPluginBase):
    """
        Plugin class for the latest news
    """
    model = NoticiasPlugin
    name = _('Noticias')
    render_template = "noticias/index.html"

    def render(self, context, instance, placeholder):
        """
            Render the latest news
        """
        latest = Noticia.objects.all()
        latest = latest[:6] if len(latest) > 6 else latest


        context.update({
            'instance': instance,
            'latest': latest,
            'placeholder': placeholder,
        })
        return context


plugin_pool.register_plugin(CMSNoticiasPlugin)
